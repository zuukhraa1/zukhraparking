
public class Car  {
    private int steps;
    private String number;
    public Car(int steps, String number){
        this.steps = steps;
        this.number = number;
    }


    public int getSteps(){
        return this.steps;
    }

    public void step(){
        this.steps--;
    }

}
