import java.util.ArrayList;
import java.util.List;

public class Parking {

    private int capacity;
    private List<Car> cars;
    private List<Car> waiting;

    public Parking(int capacity) {
        this.capacity = capacity;
        this.cars = new ArrayList<>();
        this.waiting = new ArrayList<>();
    }

    public void addCar(Car car) {
        this.cars.add(car);
        capacity = capacity - 1;
    }

    public void addCars(List<Car> cars) {
        for (var car : cars) {
            addCar(car);
        }
    }

    private void check() {
        var toRemove = new ArrayList<Car>();
        for (var car : cars) {
            if (car.getSteps() == 0) {
                toRemove.add(car);
                capacity++;
            }
        }
        for (var car: toRemove){
            cars.remove(car);
        }
        for (var car : waiting) {
            if (capacity == 0) {
                getWaitingInfo();
                break;
            }
            cars.add(car);
            waiting.remove(car);
        }
    }

    public void step() {
        for (var car : cars) {
            car.step();
        }
        check();
    }

    public void getWaitingInfo() {
        System.out.println("Парковка занята, свободное место появится через " + getMin() + " ходов");
    }

    public void getInfo() {
        System.out.printf("Занято мест %d \n", cars.size());
        System.out.printf("Свободно мест %d \n", capacity);
        if (capacity == 0) {
            System.out.printf("Cвободное место появится через %d ходов \n", getMin());
        }
    }

    private int getMin() {
        int min = 11;
        for (var car : cars) {
            if (car.getSteps() < min) {
                min = car.getSteps();
            }
        }
        return min;
    }

    public void clear(int capacity){
        this.capacity = capacity;
        this.cars.clear();;
    }
}
