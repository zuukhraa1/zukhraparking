import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        System.out.print("Введите вместимость парковки: ");
        var in = new Scanner(System.in);
        var capacity = in.nextInt();
        var parking = new Parking(capacity);
        parking.addCars(generateCars(capacity));
        String s;

        while (true) {
            System.out.print("Введите одну из команд(step,info,clear,exit) : ");
            s = in.next();
            switch (s) {
                case "step":
                    parking.step();
                    parking.addCars(generateCars(capacity));
                    break;
                case "info":
                    parking.getInfo();
                    break;
                case "clear":
                    parking.clear(capacity);
                    break;
                case "exit":
                    return;

            }
        }
    }

    private static List<Car> generateCars(int capacity) {
        List<Car> cars = new ArrayList<>();
        var random = new Random();
        var n = random.nextInt(capacity / 2);
        for (var i = 0; i < n; i++) {
            cars.add(new Car(random.nextInt(10) + 1, String.valueOf(random.nextInt(8999) + 1000)));
        }

        return cars;
    }
}
